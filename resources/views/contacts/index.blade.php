<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('style.css')}}">
</head>
<body>
    <div class="container">
        <div class="list-group" >
            <div class="column-header" style="display:flex; justify-content:space-evenly;">
                <span class="headings">First Name</span>
                <span class="headings">Last Name</span>
                <span class="headings">Date of Birth</span>
            </div>
        @foreach($formfields as $field)
        <li class="contact-row flex">
            <a href="#" class="list-group-item list-group-item-action"> {{$field->first_name}}</a>
            <a href="#" class="list-group-item list-group-item-action">{{$field->last_name}}</a>
            <a href="#" class="list-group-item list-group-item-action">{{$field->date}}</a>

            <form action="{{route('contacts.destroy',$field->id)}}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            <a href="{{route('contacts.show',$field->id)}}" type="button" class="btn btn-success" style="height : 38px;">Show<a>
        </li>
        @endforeach
        </div>
    </div>

    <script src="{{ asset('app.js') }}"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>


