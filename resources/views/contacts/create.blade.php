<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ContactManager</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('style.css')}}">
</head>
<body>
    <div class="container m-4 p-4" style="border:2px solid black;">
        <div><h2>Registration Form</h2></div>
        <form action="{{route('contacts.store')}}" method="POST">
            @csrf
            <div class="form-row flex">
              <div class="form-group col-md-5">
                <label for="first_name">First Name</label>
                <input type="text" class="form-control" name="first_name" placeholder="first_name" >
              </div>
              <div class="form-group col-md-5 mx-1">
                <label for="last_name">Last Name</label>
                <input type="text" class="form-control" name="last_name" placeholder="last_name">
              </div>
              <div class="form-group col-md-2">
                <label for="date">Date of Birth</label>
                <input type="date" class="form-control" name="date">
              </div>
            </div>
            <div class="form-row flex">
                <div class="form-group col-md-6 mx-1">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="email">
                </div>
                <div class="form-group col-md-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="password">
                </div>
            </div>
            <div class="form-group ">
              <label for="address">Address</label>
              <input type="text" class="form-control" name="address" placeholder="Enter your address">
            </div>
            <div class="form-group ">
              <label for="address2">Address 2</label>
              <input type="text" class="form-control" name="address2" placeholder="Enter your second address ">
            </div>
            <div class="form-row flex">
              <div class="form-group col-md-5">
                <label for="country">Country</label>
                <select name="country" class="form-control">
                    <option selected></option>
                    <option>India</option>
                  </select>
              </div>
              <div class="form-group col-md-4 mx-1">
                <label for="state">State</label>
                <select name="state" class="form-control">
                  <option selected></option>
                  <option>Maharashtra</option>
                </select>
              </div>
              <div class="form-group col-md-3 ">
                <label for="status">Status</label>
                <select name="status" class="form-control">
                    <option selected></option>
                    <option>InActive</option>
                    <option>Active</option>
                  </select>
              </div>
            </div>
            <button type="submit" class="btn btn-primary my-2">Create Contact</button>
          </form>
    </div>

    <script src="{{ asset('app.js') }}"></script>
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
