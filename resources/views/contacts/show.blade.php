<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('style.css')}}">
</head>
<body>
    <div class="container m-5" style="display:flex;flex-direction:column;height:600px;justify-content:center;">
        <h3>Contact Details</h3>
        <div class="list-group" >
            <ul class=" list-group" style="flex-direction:column;">
                <li class="list-group-item">First Name : <strong>{{$formfields->first_name}}</strong></li>
                <li class="list-group-item">Last Name  : <strong>{{$formfields->last_name}}</strong></li>
                <li class="list-group-item">Email  : <strong>{{$formfields->email}}</strong></li>
                <li class="list-group-item">Password  : <strong>{{$formfields->password}}</strong></li>
                <li class="list-group-item">Address  : <strong>{{$formfields->address}}</strong></li>
                <li class="list-group-item">Address2 : <strong>{{$formfields->address2}}</strong></li>
                <li class="list-group-item">Created_on : <strong>{{$formfields->created_at}}</strong></li>
                <li class="list-group-item">Updated_on : <strong>{{$formfields->updated_at}}</strong></li>
                <a href="{{route('contacts.edit',$formfields->id)}}" class="btn btn-primary" role="button">Edit</a>
                <a href="{{route('contacts.index')}}" class="btn btn-secondary" role="button">BACK</a>


            </ul>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
