<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Array_;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Contact $contact)
    {
        $formfields = $contact->all();

        return view('contacts.index', compact('formfields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formfields = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'date' => 'required',
            'email' => ['required','email'],
            'password' => 'required',
            'address' => 'required',
            'address2' => 'required',
            'country' => ['required'],
            'state' => ['required'],
            'status' => ['required'],
        ]);
        $formfields = $request->except(['_token']);
        Contact::create($formfields)->with('message','Stored Successfully');
        // $data = Contact::create($formfields);
        return redirect()->route('contacts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $formfields = Contact::find($id);
        return view('contacts.show', compact('formfields'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $formfields = Contact::find($id);
        return view('contacts.edit',compact('formfields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
       $formfields = $request->all();
       $contact = Contact::find($id);
       $contact->first_name = $formfields["first_name"];
       $contact->last_name = $formfields["last_name"];
       $contact->date = $formfields["date"];
       $contact->email = $formfields["email"];
       $contact->password = $formfields["password"];
       $contact->address = $formfields["address"];
       $contact->address2 = $formfields["address2"];
       $contact->country = $formfields["country"];
       $contact->state = $formfields["state"];
       $contact->save();

       return redirect()->route('contacts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $formfields = Contact::find($id);
       $formfields->delete();
       return redirect()->route('contacts.index');
    }
}
